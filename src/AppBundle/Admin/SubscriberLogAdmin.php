<?php
namespace AppBundle\Admin;

use AppBundle\Entity\SubscriberLog;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SubscriberLogAdmin extends Admin
{
//    public function getNewInstance()
//    {
//        return (new SubscriberLog())
//            ->setCreatedAt(new \DateTime())
//        ;
//    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('status');

        $sources = ['A1', 'SDK'];
        $formMapper->add('source',
            'choice',
            array(
                'choices' => array_combine($sources, $sources),
            )
        );

        $formMapper->add('date', 'text', ['disabled' => true]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('status');
        $datagridMapper->add('source');
        $datagridMapper->add('createdAt');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('status');
        $listMapper->add('source');
        $listMapper->add('createdAt');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
    }
}
