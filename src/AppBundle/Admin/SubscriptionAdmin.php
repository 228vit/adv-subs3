<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SubscriptionAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name');
        $formMapper->add('serviceId');
        $formMapper->add('serviceSecretKey');
        $formMapper->add('minCalls');
        $formMapper->add('minSms');
        $formMapper->add('minContacts');
        $formMapper->add('priority');
        $formMapper->add('silencePeriod');


        $formMapper->add('subscriptionOperators', 'sonata_type_collection',
            array(
                'required' => false,
                'by_reference' => false,
            ),
            array(
                'edit' => 'inline',
                'inline' => 'table',
            )
        );

        $em = $this->modelManager->getEntityManager('AppBundle\Entity\Application');
        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('AppBundle:Application', 'c')
            ->orderBy('c.code', 'ASC')
        ;

        $formMapper->add('subscriptionApplications', 'sonata_type_model',
            array(
                'by_reference' => false,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'query' => $query,
            )
        );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('serviceId');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->add('serviceId');
        $listMapper->add('priority');
        $listMapper->add('silencePeriod');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
        // OR remove all route except named ones
//        $collection->clearExcept(array('list', 'show'));
    }
}
