<?php

namespace AppBundle\Admin\ModelManager;

use AppBundle\Entity\Repositories\OperatorRepository;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;

class OperatorModelManager extends ModelManager
{

    /**
     * @param User[] $userList
     * @return mixed
     */
    public function createListQueryBuilderByUserList($userList)
    {
        return $this->getRepository()->createStatusNeqListQueryBuilderByUserList($this->getDeletedStatus(), $userList);
    }

}