<?php

namespace AppBundle\Admin\ModelManager;

use Adviator\SymfonyExtensions\Utils\Assert;
use AppBundle\Enum\Interfaces\IDeletedStatus;
use Doctrine\DBAL\DBALException;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class BaseModelManager extends ModelManager
{
    /** @var string */
    private $class;

    /** @var IDeletedStatus */
    private $deletedStatus;

    public function __construct(RegistryInterface $registry, $class, IDeletedStatus $deletedStatus)
    {
        parent::__construct($registry);

        $this->class = $class;
        $this->deletedStatus = $deletedStatus;
    }

    /**
     * @return IDeletedStatus
     */
    public function getDeletedStatus()
    {
        return $this->deletedStatus;
    }

    final public function delete($object)
    {
        Assert::isInstance($object, $this->getClass());

        $this->doDelete($object);
        $this->getEntityManager($object)->persist($object);
        $this->getEntityManager($object)->flush($object);
    }

    final public function batchDelete($class, ProxyQueryInterface $queryProxy)
    {
        $queryProxy->select('DISTINCT '.$queryProxy->getRootAlias());

        try {
            $entityManager = $this->getEntityManager($class);

            $i = 0;
            foreach ($queryProxy->getQuery()->iterate() as $pos => $object) {
                Assert::isInstance($object[0], $this->getClass());
                $this->doDelete($object[0]);

                if ((++$i % 20) == 0) {
                    $entityManager->flush();
                    $entityManager->clear();
                }
            }

            $entityManager->flush();
            $entityManager->clear();
        } catch (\PDOException $e) {
            throw new ModelManagerException('', 0, $e);
        } catch (DBALException $e) {
            throw new ModelManagerException('', 0, $e);
        }
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    final public function getRepository()
    {
        return $this->getEntityManager($this->getClass())->getRepository($this->getClass());
    }

    /**
     * @return string
     */
    final protected function getClass()
    {
        return $this->class;
    }

    /**
     * @param object $object
     * @return void
     */
    final protected function doDelete($object)
    {
        $object->setStatus($this->deletedStatus);
    }
}