<?php
namespace AppBundle\Admin;

use Adviator\SymfonyExtensions\Form\Type\Enumeration;
use Adviator\SymfonyExtensions\Form\Type\NamedList;
use AppBundle\Entity\Subscriber;
use \AppBundle\Enum\SubscriberStatus;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SubscriberAdmin extends Admin
{

    public function getNewInstance()
    {
        return (new Subscriber())
            ->setA1Status(SubscriberStatus::init())
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('subscriberId');
        $formMapper->add('subscription');
        $formMapper->add('phone');
        $formMapper->add('imei');
        $formMapper->add('imsi');
        $formMapper->add('appId');

        $statuses = ['INIT', 'CONFIRM', 'SMS', 'PAYMENT', 'STOP'];
        $formMapper->add('a1Status',
            'choice',
            array(
                'choices' => array_combine($statuses, $statuses),
            )
        );
        $formMapper->add('sdkStatus');

        $formMapper->add('logs', 'sonata_type_collection',
            array(
                'required' => false,
                'by_reference' => false,
            ),
            array(
                'edit' => 'inline',
                'inline' => 'table',
            )
        );

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('subscriberId');
        $datagridMapper->add('subscription');
        $datagridMapper->add('phone');
        $datagridMapper->add('imei');
        $datagridMapper->add('imsi');
        $datagridMapper->add('appId');
        $datagridMapper->add('a1Status');
        $datagridMapper->add('sdkStatus');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('subscriberId');
        $listMapper->addIdentifier('phone');
        $listMapper->addIdentifier('imei');
        $listMapper->addIdentifier('imsi');
        $listMapper->add('appId');
        $listMapper->add('a1Status');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
        // OR remove all route except named ones
//        $collection->clearExcept(array('list', 'show'));
    }
}
