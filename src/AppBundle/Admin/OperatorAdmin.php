<?php
namespace AppBundle\Admin;

use AppBundle\Admin\ModelManager\OperatorModelManager;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class OperatorAdmin extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name');
        $formMapper->add('a1Code');

        $em = $this->modelManager->getEntityManager('AppBundle\Entity\OperatorMncCode');
        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('AppBundle:Application', 'c')
            ->orderBy('c.code', 'ASC')
        ;

        $formMapper->add('mncCodes', 'sonata_type_model',
            array(
                'by_reference' => false,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'query' => $query,
            )
        );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
//        $datagridMapper->add('mnc_codes');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
//        $listMapper->add('mnc_codes');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('delete');
        // OR remove all route except named ones
//        $collection->clearExcept(array('list', 'show'));
    }
}
