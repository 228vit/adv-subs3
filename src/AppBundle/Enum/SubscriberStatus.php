<?php

namespace AppBundle\Enum;

use Adviator\SymfonyExtensions\Base\Enumeration;

final class SubscriberStatus extends Enumeration
{
    const
        INIT = 1,
        CONFIRM = 2,
        SMS = 3,
        PAYMENT = 4,
        STOP = 5
    ;

    protected $names = [
        self::INIT => 'Init',
        self::CONFIRM => 'Confirm',
        self::SMS => 'SMS',
        self::PAYMENT => 'Payment',
        self::STOP => 'Stop',
    ];

    /**
     * @return bool
     */
    public function isInit()
    {
        return $this->getId() == self::INIT;
    }

    /**
     * @return bool
     */
    public function isConfirm()
    {
        return $this->getId() == self::CONFIRM;
    }

    /**
     * @return bool
     */
    public function isSms()
    {
        return $this->getId() == self::SMS;
    }

    /**
     * @return bool
     */
    public function isPayment()
    {
        return $this->getId() == self::PAYMENT;
    }

    /**
     * @return bool
     */
    public function isStop()
    {
        return $this->getId() == self::STOP;
    }


    /**
     * @return SubscriberStatus
     */
    public static function init()
    {
        return new self(self::INIT);
    }

    /**
     * @return SubscriberStatus
     */
    public static function confirm()
    {
        return new self(self::CONFIRM);
    }

    /**
     * @return SubscriberStatus
     */
    public static function sms()
    {
        return new self(self::SMS);
    }

    /**
     * @return SubscriberStatus
     */
    public static function payment()
    {
        return new self(self::PAYMENT);
    }

    /**
     * @return SubscriberStatus
     */
    public static function stop()
    {
        return new self(self::STOP);
    }
}