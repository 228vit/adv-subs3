<?php

namespace AppBundle\Enum;

use \Adviator\SymfonyExtensions\Base\Enumeration;
use \Adviator\SymfonyExtensions\Utils\Assert;
use \Symfony\Component\Security\Core\Role\RoleInterface;

final class Role extends Enumeration implements RoleInterface
{
    const
        SUPERUSER = 1,
        ADMIN = 2,
        WEBMASTER = 3
    ;

    protected $names = [
        self::SUPERUSER => 'Superuser',
        self::ADMIN => 'Admin',
        self::WEBMASTER => 'Webmaster',
    ];

    private static $symfonyRoleNameMap = [
        self::SUPERUSER => 'ROLE_SUPERUSER',
        self::ADMIN => 'ROLE_ADMIN',
        self::WEBMASTER => 'ROLE_WEBMASTER',
    ];

    public function getRole()
    {
        return self::$symfonyRoleNameMap[$this->getId()];
    }

    /**
     * @param string $symfonyRole
     * @return Role
     */
    public static function createBySymfonyRole($symfonyRole)
    {
        $map = array_flip(self::$symfonyRoleNameMap);
        Assert::isTrue(array_key_exists($symfonyRole, $map), 'Unsupported Symfony role ' . $symfonyRole);

        return new self($map[$symfonyRole]);
    }

    /**
     * @return Role
     */
    public static function superuser()
    {
        return new self(self::SUPERUSER);
    }

    /**
     * @return Role
     */
    public static function admin()
    {
        return new self(self::ADMIN);
    }

    /**
     * @return Role
     */
    public static function webmaster()
    {
        return new self(self::WEBMASTER);
    }

    /**
     * @return bool
     */
    public function isWebmaster()
    {
        return $this->getId() == self::WEBMASTER;
    }

    /**
     * @return bool
     */
    public function isSuperuser()
    {
        return $this->getId() == self::SUPERUSER;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->getId() == self::ADMIN;
    }

    /**
     * @return self[]
     */
    public function getHandledRoleList()
    {
        switch ($this->getId()) {
            case self::SUPERUSER:
                return [self::superuser(), self::admin(), self::webmaster()];
            case self::ADMIN:
                return [self::webmaster()];
        }

        Assert::isUnreachable();
    }

    /**
     * @param Role $role
     * @return bool
     */
    public function canHandle(Role $role)
    {
        return in_array($role, $this->getHandledRoleList());
    }
}