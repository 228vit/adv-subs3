<?php

namespace AppBundle\Enum\Interfaces;

use Adviator\SymfonyExtensions\Base\Enumeration;
use Adviator\SymfonyExtensions\Base\Named;

interface IDeletedStatus extends Named
{
    /**
     * @return Enumeration
     */
    public static function deleted();
}