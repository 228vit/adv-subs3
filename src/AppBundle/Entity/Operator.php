<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Constraint;

/**
 * Оператор сотовой связи
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repositories\OperatorRepository")
 */
class Operator
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Constraint\NotBlank()
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * Много кодов - один оператор
     * @ORM\OneToMany(targetEntity="OperatorMncCode", mappedBy="operator")
     */
    private $mncCodes;

    /**
     * A1Billingg codes example:
     *  1 - MTS
     *  2 - Beeline
     *  3 - Megafon
     *  4 - Tele2
     * @Constraint\NotBlank()
     * @ORM\Column(type="integer")
     */
    private $a1Code;

    public function __construct()
    {
        $this->mncCodes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getMncCodes()
    {
        return $this->mncCodes;
    }

    /**
     * @param mixed $mncCodes
     */
    public function setMncCodes($mncCodes)
    {
        $this->mncCodes = $mncCodes;
    }

    /**
     * @return mixed
     */
    public function getA1Code()
    {
        return $this->a1Code;
    }

    /**
     * @param mixed $a1Code
     */
    public function setA1Code($a1Code)
    {
        $this->a1Code = $a1Code;
    }




    /**
     * Add mncCode
     *
     * @param \AppBundle\Entity\OperatorMncCode $mncCode
     *
     * @return Operator
     */
    public function addMncCode(\AppBundle\Entity\OperatorMncCode $mncCode)
    {
        $this->mncCodes[] = $mncCode;

        return $this;
    }

    /**
     * Remove mncCode
     *
     * @param \AppBundle\Entity\OperatorMncCode $mncCode
     */
    public function removeMncCode(\AppBundle\Entity\OperatorMncCode $mncCode)
    {
        $this->mncCodes->removeElement($mncCode);
    }
}
