<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Подписчики
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repositories\SubscriberRepository")
 * @ORM\Table(name="subscriber", uniqueConstraints={@ORM\UniqueConstraint(name="subscriberId",
    columns={"id"})})
 */

class Subscriber {
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Много подписчиков - одна подписка
     *
     * @var Subscription
     * @ORM\ManyToOne(targetEntity="Subscription", cascade={"persist"})
     * @ORM\JoinColumn(name="subscription_id", referencedColumnName="id")
     */
    private $subscription;

    /**
     * Много логов - один подписчик
     * @ORM\OneToMany(targetEntity="SubscriberLog", mappedBy="subscriber", cascade={"persist"}, orphanRemoval=true)
     */
    private $logs;

    /**
     * Много Подписчиков - один Оператор
     * @ORM\ManyToOne(targetEntity="Operator", cascade={"persist"})
     * @ORM\JoinColumn(name="operator_id", referencedColumnName="id")
     */
    private $operator;

    /**
     * В админке А1 нечто вроде номера подписки (4250 ...)
     * @var integer
     * @ORM\Column(type="integer", nullable=true, length=5)
     */
    private $serviceId;

    /**
     * ID подписчика из А1/Оператора
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $subscriberId;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $phone;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $imei;

    /**
     * @var integer
     * @ORM\Column(type="string", nullable=true)
     */
    private $imsi;

    /**
     * SDK присылает параметр appid - уникальный номер зарегистрированного в А1 моб.приложения
     * appid - приложение (например игрушка)
     * service_id - сама подписка в A1
     * в нашей базе это Application->code
     * храним как есть, не привязываясь к таблице Appliication
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $appId;

    /**
     * Последний статус из А1, полный лог смотрим в логе
     *
     * @var string
     * @ORM\Column(name="a1_status", type="string", columnDefinition="ENUM('INIT', 'CONFIRM', 'SMS', 'PAYMENT', 'STOP')")
     */
    private $a1Status;

    /**
     * Статус на основе присланных от моб.устройства данных
     * @var string
     * @ORM\Column(name="sdk_status", type="string", nullable=true, length=32)
     */
    private $sdkStatus;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;


    public function __construct()
    {
        $this->logs = new ArrayCollection();
    }

    public function __toString()
    {
        return sprintf('id: %s, imei: %s, appId: %s, phone: %s, status: %s', $this->getId(), $this->getImei(), $this->getappId(), $this->getPhone(), $this->getStatus());
    }

    public function getStatus()
    {
        $status = [];
        if ($this->getA1Status()) {
            $status[] = $this->getA1Status();
        }

        if ($this->getSdkStatus()) {
            $status[] = $this->getSdkStatus();
        }

        return implode(', ', $status);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return mixed
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param mixed $logs
     */
    public function setLogs($logs)
    {
        $this->logs = $logs;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param mixed $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param int $serviceId
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return int
     */
    public function getSubscriberId()
    {
        return $this->subscriberId;
    }

    /**
     * @param int $subscriberId
     */
    public function setSubscriberId($subscriberId)
    {
        $this->subscriberId = $subscriberId;
    }

    /**
     * @return int
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * @param int $imei
     */
    public function setImei($imei)
    {
        $this->imei = $imei;
    }

    /**
     * @return int
     */
    public function getImsi()
    {
        return $this->imsi;
    }

    /**
     * @param int $imsi
     */
    public function setImsi($imsi)
    {
        $this->imsi = $imsi;
    }

    /**
     * @return int
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param int $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return string
     */
    public function getA1Status()
    {
        return $this->a1Status;
    }

    /**
     * @param string $a1Status
     */
    public function setA1Status($a1Status)
    {
        $this->a1Status = $a1Status;
    }

    /**
     * @return string
     */
    public function getSdkStatus()
    {
        return $this->sdkStatus;
    }

    /**
     * @param string $sdkStatus
     */
    public function setSdkStatus($sdkStatus)
    {
        $this->sdkStatus = $sdkStatus;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

//        return $this;




    /**
     * Add log
     *
     * @param \AppBundle\Entity\SubscriberLog $log
     *
     * @return Subscriber
     */
    public function addLog(\AppBundle\Entity\SubscriberLog $log)
    {
        $log->setCreatedAt(new \DateTime());
        $log->setSubscriber($this);
        $this->logs[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AppBundle\Entity\SubscriberLog $log
     */
    public function removeLog(\AppBundle\Entity\SubscriberLog $log)
    {
        $this->logs->removeElement($log);
    }
}
