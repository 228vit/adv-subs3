<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Constraint;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repositories\SubscriptionRepository")
 */
class Subscription
{
    const STATE_ACTIVE = 1;
    const STATE_STOPED = 2;
    const STATE_DELETED = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * Одна подписка - Много подключенных операторов с настройками
     *
     * @var Collection
     * @ORM\OneToMany(targetEntity="SubscriptionOperator", mappedBy="subscription", cascade={"persist"}, orphanRemoval=true)
     */
    private $subscriptionOperators;

    /**
     * Много подписок - много приложений
     *
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Application", inversedBy="subscriptions", cascade={"persist"})
     * @ORM\JoinTable(
     *  name="subscription_application",
     *  joinColumns={
     *      @ORM\JoinColumn(name="subscription_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="application_id", referencedColumnName="id")
     *  }
     * )
     */
    private $subscriptionApplications;


    /**
     * Параметр id из админки a1
     * @ORM\Column(type="string")
     */
    private $serviceId;

    /**
     * Параметр secret_key из админки a1
     * @ORM\Column(type="string")
     */
    private $serviceSecretKey;

    /**
     * @Constraint\GreaterThan (
     *  value = -1,
     *  message = "Значение должно быть положительным"
     * )
     * @ORM\Column(type="integer")
     */
    private $minCalls;

    /**
     * @Constraint\GreaterThan (
     *  value = -1,
     *  message = "Значение должно быть положительным"
     * )
     * @ORM\Column(type="integer")
     */
    private $minSms;

    /**
     * @Constraint\GreaterThan (
     *  value = -1,
     *  message = "Значение должно быть положительным"
     * )
     * @ORM\Column(type="integer")
     */
    private $minContacts;

    /**
     * @Constraint\Range(
     *      min = 1,
     *      max = 10,
     *      minMessage = "Приоритет должен быть не менее {{ limit }}",
     *      maxMessage = "Приоритет должен быть не более {{ limit }}"
     * )
     * @ORM\Column(type="smallint")
     */
    private $priority;

    /**
     * Пауза в часах, перед тем, как выдать подписчику его подписку
     * @Constraint\Range(
     *      min = 1,
     *      max = 48,
     *      minMessage = "Период молчания быть не менее {{ limit }}",
     *      maxMessage = "Период молчания быть не более {{ limit }}"
     * )
     * @ORM\Column(type="smallint")
     */
    private $silencePeriod;

    /**
     * Статус подписки, см констатнты
     * @ORM\Column(type="smallint")
     */
    private $state;

    public function __toString()
    {
        return sprintf('%s: %s', $this->getId(), $this->getName());
    }

    public function __construct()
    {
        $this->state = 1;
        $this->subscriptionApplications = new ArrayCollection();
        $this->subscriptionOperators = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getSubscriptionOperators()
    {
        return $this->subscriptionOperators;
    }

    /**
     * @param Collection $subscriptionOperators
     */
    public function setSubscriptionOperators($subscriptionOperators)
    {
        $this->subscriptionOperators = $subscriptionOperators;
    }

    /**
     * @return Collection
     */
    public function getSubscriptionApplications()
    {
        return $this->subscriptionApplications;
    }

    /**
     * @param Collection $subscriptionApplications
     */
    public function setSubscriptionApplications($subscriptionApplications)
    {
        $this->subscriptionApplications = $subscriptionApplications;
    }

    /**
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param mixed $serviceId
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return mixed
     */
    public function getServiceSecretKey()
    {
        return $this->serviceSecretKey;
    }

    /**
     * @param mixed $serviceSecretKey
     */
    public function setServiceSecretKey($serviceSecretKey)
    {
        $this->serviceSecretKey = $serviceSecretKey;
    }

    /**
     * @return mixed
     */
    public function getMinCalls()
    {
        return $this->minCalls;
    }

    /**
     * @param mixed $minCalls
     */
    public function setMinCalls($minCalls)
    {
        $this->minCalls = $minCalls;
    }

    /**
     * @return mixed
     */
    public function getMinSms()
    {
        return $this->minSms;
    }

    /**
     * @param mixed $minSms
     */
    public function setMinSms($minSms)
    {
        $this->minSms = $minSms;
    }

    /**
     * @return mixed
     */
    public function getMinContacts()
    {
        return $this->minContacts;
    }

    /**
     * @param mixed $minContacts
     */
    public function setMinContacts($minContacts)
    {
        $this->minContacts = $minContacts;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getSilencePeriod()
    {
        return $this->silencePeriod;
    }

    /**
     * @param mixed $silencePeriod
     */
    public function setSilencePeriod($silencePeriod)
    {
        $this->silencePeriod = $silencePeriod;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }



    /**
     * Add subscriptionOperator
     *
     * @param \AppBundle\Entity\SubscriptionOperator $subscriptionOperator
     *
     * @return Subscription
     */
    public function addSubscriptionOperator(\AppBundle\Entity\SubscriptionOperator $subscriptionOperator)
    {
        $subscriptionOperator->setSubscription($this);
        $this->subscriptionOperators[] = $subscriptionOperator;

        return $this;
    }

    /**
     * Remove subscriptionOperator
     *
     * @param \AppBundle\Entity\SubscriptionOperator $subscriptionOperator
     */
    public function removeSubscriptionOperator(\AppBundle\Entity\SubscriptionOperator $subscriptionOperator)
    {
        $this->subscriptionOperators->removeElement($subscriptionOperator);
    }

    /**
     * Add subscriptionApplication
     *
     * @param \AppBundle\Entity\Application $subscriptionApplication
     *
     * @return Subscription
     */
    public function addSubscriptionApplication(\AppBundle\Entity\Application $subscriptionApplication)
    {
        $this->subscriptionApplications[] = $subscriptionApplication;

        return $this;
    }

    /**
     * Remove subscriptionApplication
     *
     * @param \AppBundle\Entity\Application $subscriptionApplication
     */
    public function removeSubscriptionApplication(\AppBundle\Entity\Application $subscriptionApplication)
    {
        $this->subscriptionApplications->removeElement($subscriptionApplication);
    }
}
