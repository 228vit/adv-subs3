<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Constraint;

/**
 * Подписка имеет связи с операторами и настройки по каждому оператору
 * Class SubscriptionOperator
 * @package AppBundle\Entity
 * @ORM\Entity
 */
class SubscriptionOperator
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Много настроек - одна подписка
     *
     * @var Subscription
     * @ORM\ManyToOne(targetEntity="Subscription")
     * @ORM\JoinColumn(name="subscription_id", nullable=false, referencedColumnName="id")
     */
    private $subscription;

    /**
     * Много операторов - одна настройка на подписке
     *
     * @var Operator
     * @ORM\ManyToOne(targetEntity="Operator")
     * @ORM\JoinColumn(name="operator_id", nullable=false, referencedColumnName="id")
     */
    private $operator;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=2048)
     * @Constraint\NotBlank()
     * @Constraint\Url()
     * @Constraint\Length(max="2048")
     */
    private $url;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=2048)
     * @Constraint\NotBlank()
     * @Constraint\Length(max="2048")
     */
    private $js;

    public function __toString()
    {
        return sprintf("%s: %s", $this->getOperator(), $this->getSubscription());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param Subscription $subscription
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return Operator
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param Operator $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getJs()
    {
        return $this->js;
    }

    /**
     * @param string $js
     */
    public function setJs($js)
    {
        $this->js = $js;
    }


}
