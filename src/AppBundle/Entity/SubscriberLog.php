<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repositories\SubscriberLogRepository")
 * @ORM\Table(name="subscriber_log")
 */
class SubscriberLog
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Много логов - один подписчик
     * @var Subscriber
     * @ORM\ManyToOne(targetEntity="Subscriber", inversedBy="logs")
     * @ORM\JoinColumn(name="subscriber_id", referencedColumnName="id")
     */
    private $subscriber;

    /**
     * Статус из А1Биллинг или MobileSDK - 'INIT', 'CONFIRM', 'SMS', 'PAYMENT', 'STOP'...
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * Откуда пришел лог
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('SDK', 'A1')")
     */
    private $source;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $extra;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Subscriber
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * @param Subscriber $subscriber
     */
    public function setSubscriber($subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param string $extra
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;
    }

    public function getDate()
    {
        $created = $this->getCreatedAt();

        return ($created instanceof \DateTime) ? $created->format('d.m.Y H:i:s') : '---';
    }

}
