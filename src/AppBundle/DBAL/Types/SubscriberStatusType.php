<?php

namespace AppBundle\DBAL\Types;

use \Adviator\SymfonyExtensions\DBAL\Types\EnumerationType;
use \AppBundle\Enum\SubscriberStatus as Enum;

final class SubscriberStatusType extends EnumerationType
{
    protected function spawnEnum()
    {
        return new Enum();
    }
}